## Get your own Smart Chatbot

[Siba Bot](https://siba.chat) is a smart chatbot that you can embed in your Hugo Site. 
It is powered by [Siba](https://siba.ai) and you can get your own personal bot.

Head over to the [Siba Chat](https://siba.chat) site to get your own bot today.
